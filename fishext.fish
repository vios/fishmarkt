function fishext
    omf search -p '^.*$' | fzf -d ' ' --with-nth=1 --preview='echo {3..}' --cycle --border --layout=reverse-list --preview-window=right:wrap --header 'Install fish plugin' --color 'fg:188,bg:233,hl:103,fg+:222,bg+:234,hl+:104' --color 'info:183,prompt:110,spinner:107,pointer:167,marker:215'
end
