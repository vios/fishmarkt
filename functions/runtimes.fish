function runtimes
    set_color blue -o
    printf '== Runtime info ==\n'
    set_color green -o;printf '  > Python:\t';   set_color normal;printf ' %s\n' (python --version)
    set_color green -o;printf '  > Node.js:\t';  set_color normal;printf ' %s\n' (node --version)
    set_color green -o;printf '  > Ruby:\t';     set_color normal;printf ' %s\n' (ruby --version)
    set_color green -o;printf '  > Lua:\t';      set_color normal;printf ' %s\n' (lua -v)
    set_color green -o;printf '  > Rust:\t';     set_color normal;printf ' %s\n' (rustc --version)
    set_color normal
end

