# Defined in /home/docbraun/.config/fish/functions/xwayrun.fish @ line 2
function xwayrun
	echo "Starting wayland application..."
    set -x WAYLAND_DISPLAY "wayland-0"
    for arg in $argv
        echo $arg
        switch $arg
        case "qt"
            echo "Enabling Qt support"
            set -x QT_QPA_PLATFORMTHEME "wayland"
        case "qtext"
            echo "Enabling Qt Desktop extensions"
            set -x QT_WAYLAND_SHELL_INTEGRATION "xdg-shell"
        end
    end
    exec $argv[-1]
end
