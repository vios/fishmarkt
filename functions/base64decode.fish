function base64decode
    local usage="\nUSAGE:\n\tbase64decode INPUT OUTFILE"
    if test -n $argv[1]
        panic "No input$usage"
    end
    if test -n $argv[2]
        panic "No output file$usage"
    end
    echo "Decoding base64 payload to $argv[2]..."
    echo "$argv[1]" | base64 -d > "$argv[2]"
end
