# Defined in /tmp/fish.dw1dOM/ll.fish @ line 2
function ll --description 'List contents of directory using long format'
	exa -lahm@ --git --group-directories-first --color=always --color-scale $argv
end
