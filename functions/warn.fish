function warn
    set_color yellow -o
    echo "WARNING: $argv"
    set_color normal
    return 1
end