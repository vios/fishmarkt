function runas
    echo "Executing \"$argv[2]\" as User \"$argv[1]\"..."
    if installed "sudo"
        exec sudo -u $argv[1] /bin/bash -l -c $argv[2] $argv[3..-1]
    else if installed "su"
        exec su -s /bin/bash $argv[1] -l -c $argv[2] $argv[3..-1]
    end
    panic "No su or sudo installed"
    exit 1
end