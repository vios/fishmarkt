function dec
    if test -n "$argv[1]"
        printf "%d\n" $argv[1]
    else
        print 'Usage: dec <number-to-convert>'
        return 1
    end
end