# Defined in /tmp/fish.crMC6k/funclist.fish @ line 2
function funclist
    set -l selection (functions | sd '[,]\s' '\n' | sort | tail -n+3 | fzf --layout=reverse-list --border --header='== Edit shell function ==' --ansi --preview-window=right:70% --preview="printf '\e[44m> Details <\e[0m\n\n';functions -Dv {}; printf '\n\n\e[44m> Preview <\e[0m\n\n';bat (functions -D {})")
    if test -z "$selection"
        echo "User abort"
        return 1
    end
    set -l fpath (functions -D $selection)
    if test -G "$fpath" -o -O "$fpath"
        if test -w "$fpath"
            $EDITOR "$fpath"
        else
            echo "Editing file '$fpath' failed: You don't have the write permission"
        end
    else
        echo "Editing file '$fpath' failed: You are not the owner or part of the owner group"
    end
end
