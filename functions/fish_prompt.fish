# Defined in /home/docbraun/.config/fish/functions/fish_prompt.fish @ line 2
function fish_prompt
    set_color yellow
    printf '\n%s\n' (pwd);set_color blue;
    printf ' * %s' (whoami);set_color green;
    echo -n ' -> ';set_color normal;
end
