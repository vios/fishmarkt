function hex
    if test -n "$argv[1]"
        printf "%x\n" $argv[1]
    else
        print 'Usage: hex <number-to-convert>'
        return 1
    end
end