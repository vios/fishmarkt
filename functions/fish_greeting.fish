function memtotal
    python -c "with open('/proc/meminfo') as data: size=int(data.readline().split(':')[1].replace('\t','').replace('\n','').replace(' ', '').replace('kB',''));import math;print(math.floor(size/1024))"
end

function memfree
    python -c "with open('/proc/meminfo') as data: size=int(data.readlines()[1].split(':')[1].replace('\t','').replace('\n','').replace(' ', '').replace('kB',''));import math;print(math.floor(size/1024))"
end

function fish_greeting
    set_color E64A19 -o
    printf '\n\t %s - %s\n\n' (fish --version)  (uname -ro);set_color normal;
    printf ' > System status: ';set_color green -o;printf '%s\n' (systemctl is-system-running);set_color normal;
    printf ' > RAM (total): '; set_color  blue -o; printf '%s MB' (memtotal); set_color normal; printf ' / RAM (free): '; set_color green -o; printf '%s MB\n' (memfree)
    set_color normal
    printf '\r';
end

