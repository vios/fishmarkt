# Defined in /tmp/fish.havrgY/envpath.fish @ line 1
function envpath
    set -l arglen (count $argv)
    if test $arglen -eq 0
        echo $PATH
    end
    set -U fish_user_paths $argv $fish_user_paths
end
