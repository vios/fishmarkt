function panic
    set_color red -o
    echo "PANIC: $argv"
    set_color normal
    exit 1
end