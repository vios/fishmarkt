# Path to Oh My Fish install.
set -gx OMF_PATH "$HOME/.config/fish/omf-data"
set -gx OMF_CONFIG "$HOME/.config/fish/omf-conf"

# Load Oh My Fish configuration.
source $OMF_PATH/init.fish
